# import the package
import requests
import argparse



def parse_to_defect_dojo(host, api_key, user, engagement_id, file, date, scan_type):
    headers = {
        'authorization': 'Token {}'.format(api_key),
    }

    data = {
        'scan_date': date,
        'scan_type': scan_type,
        'engagement': engagement_id,
        'active': True,
        'verified': True
    }
    file = open(file, 'rb')

    url = 'http://{}/api/v2/import-scan/'.format(host)

    response = requests.post(url, headers=headers, files={"file": file}, data=data)

    if response.status_code == 201:
        print("success upload scan")
    else:
        print("fail upload scan")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", default=None, help="Host Defectdojo", type=str)
    parser.add_argument("--api_key", default=None, help="Api Key", type=str)
    parser.add_argument("--user", default=None, help="Username Defecet Dojo", type=str)
    parser.add_argument("--engagement_id", default=None, type=int)
    parser.add_argument("--file", default=None, required=True)
    parser.add_argument("--date", default=None, required=True, type=str)
    parser.add_argument("--scan_type", default="GitLab SAST Report", type=str)

    args = parser.parse_args()
    parse_to_defect_dojo(host=args.host, api_key=args.api_key, user=args.user, engagement_id=args.engagement_id,
                         file=args.file, date=args.date, scan_type=args.scan_type)




