FROM python:3.7-slim-buster

WORKDIR /app

COPY . .

RUN pip3 install -r req.txt

CMD ["python", "parser_ddojo.py"]